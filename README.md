# Awerkon

**Introduction**  
Awerkon is an application designed to learn programming.
We are going to create application that we will treat as an engine for creating educational browser-based games.
The end goal of the project is to prepare interactive web platorm that can be used to solve various programming tasks.
New exercises will be available after completing the relevant parts of the tasks on the website.
Registered users will be able to propose their own coding or architecture ideas.

Basic application template has been created using JHipster, so we as programmers don't have to worry
about the entire application skeleton, but we can focus on implementation details
and can execute more complicated and interesting tasks.

**Tech stack**
- Java
- Angular
- SpringBoot
- Neo4j

**Installation**
1. Clone project repo on your local disc (using SSH or HTTPS)
2. Make sure you have angular package installed (version 8+)
3. Install Neo4j database driver on your local machine
   (under this link you can find instruction for the most popular systems)
4. Requests in Neo4j are authenticated by default, so you have to disable security authentication.
   To disable authentication, uncomment line `dbms.security.auth_enabled=false` in neo4j.conf file. Usually this file is in the etc folder (for linux system).
5. (Optional) In some cases, after changing the settings, a restart of the neo4j service on the local machine is required. To do so, run the following command in the terminal `sudo service neo4j restart`. You can check the status of the process with the command `sudo service neo4j status`
6. Run services in the following order: Neo4j -> SpringBoot -> Angular


**FAQ**  
On which branch should I commit my changes?  
All changes add on created by you branch. After work send merge request to the development branch.
