*  **WHAT**  
Zadanie polega na XXX

*  **HOW**  

* **ROADMAP**  
Wykonujący powinien XXX

*  **PROPOSALS**  
Do XXX można użyć biblioteki `YYY`
*  **RELATED RESOURCES**   
 ~~ TUTAJ NP LINK DO WIKI  ~~
*  **DEFINITION OF DONE**  
Aplikacja powinna prawidłowo wykonać XXX

* **STEPS**  
* [ ]  Zidentyfikowanie ilu conajmniej kroków (requestow / implementacji) wymaga przejście całego flow
* [ ] Implementacja poszczególnych kroków  

###### ***PROJECT'S WIKI***  
https://elenx.net/blank/Awerkon/wikis/home
  
  *Opis ma na celu jedynie wyjaśnienie mniej więcej na czym polega zadanie - o wszelkie szczegóły lub jeśli czegoś nie jesteś pewny pytaj na `#general`. Jeśli opis nie jest  
  wystarczająco dobry, poproś o doszczegółowienie osoby wystawiającej task*
