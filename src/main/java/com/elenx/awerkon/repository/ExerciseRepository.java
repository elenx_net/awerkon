package com.elenx.awerkon.repository;

import com.elenx.awerkon.domain.Exercise;
import java.util.List;
import java.util.Optional;
import org.neo4j.springframework.data.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExerciseRepository extends Neo4jRepository<Exercise, Integer> {
    Optional<Exercise> findById(Integer id);
    List<Exercise> findAll();
    List<Exercise> findByCategory(String category);
    List<Exercise> findByType(String type);
}
