package com.elenx.awerkon.common.enums;

public enum Degree {
    BEGINNER,
    TRAINEE,
    JUNIOR,
    MID,
    SENIOR,
    NINJA,
}
