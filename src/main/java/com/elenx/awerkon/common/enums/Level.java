package com.elenx.awerkon.common.enums;

public enum Level {
    EASY,
    MEDIUM,
    HARD,
}
