package com.elenx.awerkon.common.enums;

public enum Category {
    DEVOPS,
    FRONTEND,
    BACKEND,
    TEST,
}
