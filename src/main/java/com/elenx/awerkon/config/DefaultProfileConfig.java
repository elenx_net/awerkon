package com.elenx.awerkon.config;

import lombok.NoArgsConstructor;
import org.springframework.boot.SpringApplication;

import java.util.HashMap;
import java.util.Map;

@NoArgsConstructor
public class DefaultProfileConfig {

    private static final String SPRING_PROFILE_DEFAULT = "spring.profiles.default";

    public static void addDefaultProfile(SpringApplication app) {
        Map<String, Object> defProperties = new HashMap<>();
        defProperties.put(SPRING_PROFILE_DEFAULT, "dev");
        app.setDefaultProperties(defProperties);
    }
}
