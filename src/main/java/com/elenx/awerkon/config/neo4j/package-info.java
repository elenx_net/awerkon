/**
 * Neo4j specific configuration and required migrations.
 */
package com.elenx.awerkon.config.neo4j;
