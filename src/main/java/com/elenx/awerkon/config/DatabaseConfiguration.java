package com.elenx.awerkon.config;

import org.neo4j.springframework.data.repository.config.EnableNeo4jRepositories;
import org.springframework.context.annotation.Configuration;

/**
 * Basic database configuration for Neo4j
 */

@Configuration
@EnableNeo4jRepositories("com.elenx.awerkon.repository")
public class DatabaseConfiguration { }
