/**
 * View Models used by Spring MVC REST controllers.
 */
package com.elenx.awerkon.web.rest.vm;
