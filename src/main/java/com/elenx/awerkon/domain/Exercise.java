package com.elenx.awerkon.domain;

import java.io.Serializable;
import java.util.ArrayList;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import lombok.Getter;
import lombok.Setter;
import org.neo4j.springframework.data.core.schema.Id;
import org.neo4j.springframework.data.core.schema.Node;
import org.neo4j.springframework.data.core.schema.Property;

@Node("exercise")
@Getter
@Setter
public class Exercise extends AbstractAuditingEntity implements Serializable {
    @Id
    @Property("exercise_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Property("exercise_category")
    private String category;

    @Property("exercise_question")
    private String question;

    @Property("exercise_type")
    private String type;

    @Property("exercise_answer")
    private ArrayList<String> answer;
}
