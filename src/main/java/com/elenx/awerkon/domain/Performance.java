package com.elenx.awerkon.domain;


import com.elenx.awerkon.common.enums.Level;
import lombok.Getter;
import lombok.Setter;
import org.neo4j.springframework.data.core.schema.Id;
import org.neo4j.springframework.data.core.schema.Node;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@Node("performance")
public class Performance extends AbstractAuditingEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Integer points;

    private List<String> achievements;

    private Level level;
}
