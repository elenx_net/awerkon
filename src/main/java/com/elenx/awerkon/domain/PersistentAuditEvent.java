package com.elenx.awerkon.domain;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import javax.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.neo4j.springframework.data.core.schema.GeneratedValue;
import org.neo4j.springframework.data.core.schema.Id;
import org.neo4j.springframework.data.core.schema.Node;
import org.neo4j.springframework.data.core.schema.Property;
import org.neo4j.springframework.data.core.support.UUIDStringGenerator;
import org.springframework.data.annotation.Transient;

/**
 * Persist AuditEvent managed by the Spring Boot actuator.
 */
@Node("persistent_audit_event")
@Getter
@Setter
@EqualsAndHashCode
@ToString
public class PersistentAuditEvent implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Property("event_id")
    @GeneratedValue(UUIDStringGenerator.class)
    private String id;

    @NotNull
    private String principal;

    @Property("event_date")
    private Instant auditEventDate;

    @Property("event_type")
    private String auditEventType;

    @Transient
    private Map<String, String> data = new HashMap<>();
}
