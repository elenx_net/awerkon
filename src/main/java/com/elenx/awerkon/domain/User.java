package com.elenx.awerkon.domain;

import com.elenx.awerkon.common.EnvConstants;
import com.elenx.awerkon.common.enums.Degree;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.neo4j.springframework.data.core.schema.GeneratedValue;
import org.neo4j.springframework.data.core.schema.Id;
import org.neo4j.springframework.data.core.schema.Node;
import org.neo4j.springframework.data.core.schema.Property;
import org.neo4j.springframework.data.core.schema.Relationship;
import org.neo4j.springframework.data.core.support.UUIDStringGenerator;

/**
 * A user.
 */
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString
@Setter
@Getter
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Node("user")
public class User extends AbstractAuditingEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(UUIDStringGenerator.class)
    @EqualsAndHashCode.Include
    @Property("user_id")
    private String id;

    @NotNull
    @Pattern(regexp = EnvConstants.LOGIN_REGEX)
    @Size(min = 1, max = 50)
    @Setter(AccessLevel.NONE)
    private String login;

    @JsonIgnore
    @NotNull
    @Size(min = 60, max = 60)
    private String password;

    @Size(max = 50)
    @Property("first_name")
    private String firstName;

    @Size(max = 50)
    @Property("last_name")
    private String lastName;

    @Email
    @Size(min = 5, max = 254)
    private String email;

    private boolean activated = false;

    @Size(min = 2, max = 10)
    @Property("lang_key")
    private String langKey;

    @Size(max = 256)
    @Property("image_url")
    private String imageUrl;

    @Size(max = 20)
    @Property("activation_key")
    @JsonIgnore
    private String activationKey;

    @Size(max = 20)
    @Property("reset_key")
    @JsonIgnore
    private String resetKey;

    @Property("reset_date")
    private Instant resetDate = null;

    @JsonIgnore
    @Relationship("HAS_AUTHORITY")
    private Set<Authority> authorities = new HashSet<>();

    @Relationship("PERFORM")
    private Performance performance;

    private Degree degree = Degree.BEGINNER;

    // Lowercase the login before saving it in database
    public void setLogin(String login) {
        this.login = StringUtils.lowerCase(login, Locale.ENGLISH);
    }
}
