import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AwerkonSharedModule } from 'app/shared/shared.module';
import { HOME_ROUTE } from './home.route';
import { HomeComponent } from './home.component';

@NgModule({
  imports: [AwerkonSharedModule, RouterModule.forChild([HOME_ROUTE])],
  declarations: [HomeComponent],
})
export class AwerkonHomeModule {}
